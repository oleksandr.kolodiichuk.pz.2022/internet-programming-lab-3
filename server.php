<?php

//Перевірка, чи прийшли дані через POST
if ($_SERVER["REQUEST_METHOD"] == "POST"){
    //Отримання JSON даних від клієнта
    $json_data = file_get_contents('php://input');
    //Перетворення JSON у асоціативний масив PHP
    $students = json_decode($json_data, true);
    //Перевірка, чи успішно розкодовано JSON
    if ($students === null && json_last_error() != 0){
        // Якщо сталася помилка при розкодуванні JSON
        $response = array("success" => false, "error" => "Error decoding JSON data");
        echo json_encode($response);
        exit;
    }

    //Валідація даних студента
    foreach ($students as $student){
        //Перевірка наявності обов'язкових полів та їх коректність
        if (!isset($student['group']) || !isset($student['name']) || !isset($student['gender']) || !isset($student['birthday']) ||
            empty($student['group']) || empty($student['name']) || empty($student['gender']) || empty($student['birthday'])){
                $response = array("success" => false, "error" => "One or more fields are empty");
                echo json_encode($response);
                exit;
        }

        //Розбиваємо ім'я та прізвище на слова
        $name_parts = explode(" ", $student['name']);
        //Перевірка кожного слова в імені та прізвищі
        foreach ($name_parts as $part){
            //Перевірка, чи містить слово лише літери
            if (!preg_match("/^[a-zA-Z]+$/", $part)){
                $response = array("success" => false, "error" => "Name and surname must contain only letters!");
                echo json_encode($response);
                exit;
            }
        }
        //Розбиваємо рік народження та перевіряємо, чи він менший за 2008
        $birth_year = date("Y", strtotime($student['birthday']));
        if ($birth_year >= 2008){
            $response = array("success" => false, "error" => "Birth year must be before 2008!");
            echo json_encode($response);
            exit;
        }
    }
    //Відправка відповіді клієнту про успішне завершення
    $response = array("success" => true);
    echo json_encode($response);
} else{
    //Якщо отримано не POST-запит, відправити помилку
    $response = array("success" => false, "error" => "Invalid request method");
    echo json_encode($response);
}
?>